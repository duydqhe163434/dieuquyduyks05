﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A008.Exercise1;


Student student1 = new Student("Duy", "KS05", "Male", DateTime.Now, 20, "Thai Binh");
Student student2 = new Student("Dung", "KS05", "Male", DateTime.Now, 20, "Ha Noi");
Student student3 = new Student("Thanh", "KS05", "Male", DateTime.Now, 20, "Phu Tho");


Student student4 = new Student(name: "Thuy", @class: "KS05", gender: "FEMale", entryDate: DateTime.Now, age: 20, address: "Thach That", mark: 6, grade: "B", relationship: "Single");
Student student5 = new Student(name: "Minh", @class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 20, address: "Hai Duong", mark: 10, grade: "A", relationship: "Single");
Student student6 = new Student(name: "Tuan", @class: "KS05", gender: "Male", entryDate: DateTime.Now, age: 20, address: "Cao Bang", mark: 6, grade: "B", relationship: "Single");

//Graduate method transmission parameter
Console.WriteLine("Graduate method transmission parameter:");
Console.WriteLine(student1.Graduate(6M));

//Graduate method no parameter transmission.
Console.WriteLine("Graduate method no parameter transmission:");
Console.WriteLine(student2.Graduate());
Console.WriteLine();
//toString method no parameter transmission.
Console.WriteLine("ToString method no parameter transmission: ");
Console.WriteLine(String.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student1.ToString());
Console.WriteLine(student2.ToString());
Console.WriteLine(student3.ToString());


//toString method transmission transmission.
Console.WriteLine("ToString method no parameter transmission: ");
Console.WriteLine(String.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", "Name", "Class", "Gender", "Relationship", "Age", "Grade"));
Console.WriteLine(student4.ToString(student4.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));
Console.WriteLine(student5.ToString(student5.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));
Console.WriteLine(student6.ToString(student6.Name, student1.Class, student1.Gender, student1.Relationship, student1.Age, student1.Grade));


﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A008.Exercise1
{
    internal class Student
    {

        public string Name { get; set; }
        public string Class { get; set; }
        public string Gender { get; set; }
        public string Relationship { get; set; }
        public DateTime EntryDate { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public decimal Mark { get; set; }
        public string Grade { get; set; }

        public Student()
        {
        }

        public Student(string name, string @class, string gender, DateTime entryDate, int age, string address, string relationship = "Single", decimal mark = 0, string grade = "F")
        {
            this.Name = name;
            this.Class = @class;
            this.Gender = gender;
            this.Relationship = relationship;
            this.EntryDate = entryDate;
            this.Age = age;
            this.Address = address;
            this.Mark = mark;
            this.Grade = grade;
        }

        public string Graduate(decimal gradePoint = 0)
        {
            if (gradePoint == 0M)
            {
                Grade = "F";
            }
            if (gradePoint == 1.0M)
            {
                Grade = "D";
            }
            if (gradePoint == 2.0M)
            {
                Grade = "C";
            }
            if (gradePoint == 2.3M)
            {
                Grade = "C+";
            }
            if (gradePoint == 2.7M)
            {

                Grade = "B-";
            }
            if (gradePoint == 3.0M)
            {

                Grade = "B";
            }
            if (gradePoint == 3.3M)
            {
                Grade = "B+";
            }
            if (gradePoint == 3.7M)
            {
                Grade = "A-";
            }
            if (gradePoint == 4.0M)
            {
                Grade = "A+";
            }
            return Grade;
        }
        public string? ToString(string Name, string Class, string Gender, string Relationship, int Age, string Grade)
        {
            return string.Format("{0, -15}{1, -15}{2, -15}{3, -20}{4, -15}{5, -15}", Name, Class, Gender, Relationship, Age, Grade);
        }

    }
}
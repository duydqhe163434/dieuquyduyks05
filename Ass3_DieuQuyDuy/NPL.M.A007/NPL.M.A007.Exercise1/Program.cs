﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A007.Exercise1;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;
Console.OutputEncoding = Encoding.Unicode;

string result = string.Format("{0, -20}{1, -20}{2, -20}{3, -20}", "ISBN Number", "Book Name", "Author Name", "Publisher Name");
Console.WriteLine(result);
Book book = new Book("Harry Potter",123456789,"J.K.Rowling","Kim Dong");
Console.WriteLine(book.GetBookInformation());

Console.ReadKey();


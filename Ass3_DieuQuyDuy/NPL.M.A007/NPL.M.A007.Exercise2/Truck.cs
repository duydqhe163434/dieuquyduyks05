﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Truck : Car
    {
        public Truck(decimal speed, double regularPrice, string color,int Weight) : base(speed, regularPrice, color)
        {
            this.Weight = Weight;
        }

        public int Weight { get; set; }

        public override double GetSalePrice()
        {
            double saleprice;
            if(Weight > 2000)
            {
                saleprice =RegularPrice - RegularPrice * 0.1;
            }
            else
            {
                saleprice = RegularPrice - RegularPrice * 0.2;
            }
            return saleprice;
        }
        public void DisplayPrice()
        {
            Console.WriteLine( Color + "Truck Price: " + GetSalePrice());
        }
    }
}

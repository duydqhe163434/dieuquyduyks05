﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Sedan : Car
    {
        public int Length { get; set; }
        public Sedan(decimal speed, double regularPrice, string color, int Length) : base(speed, regularPrice, color)
        {
            this.Length = Length;
        }

       

        public override double GetSalePrice()
        {
            double res;
            if (Length > 20)
            {
                res = RegularPrice*0.95;
            }
            else
            {
                res = RegularPrice * 0.9;
            }
            return res;
        }

        public void DisplayPrice()
        {
            Console.WriteLine( Color + "Sedan Price: " + GetSalePrice());
        }
    }
}

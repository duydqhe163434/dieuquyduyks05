﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class MyOwnAutoShop
    {
        Sedan sedan1 = new Sedan(120, 10000, "Red ", 60);
        Ford ford1 = new Ford(110, 10000, "Blue ", 7, 7000);
        Truck truck1 = new Truck(100, 10000, "Black ", 80);



        public void DisplayPriceOfCars()
        {
            sedan1.DisplayPrice();
            ford1.DisplayPrice();
            truck1.DisplayPrice();


        }
    }
}

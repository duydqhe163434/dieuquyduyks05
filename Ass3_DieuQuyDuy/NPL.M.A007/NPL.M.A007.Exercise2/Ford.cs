﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A007.Exercise2
{
    internal class Ford : Car
    {
        public int Year { get; set; }
        public int ManufacturerDiscount { get; set; }

        public Ford(decimal speed, double regularPrice, string color, int Year, int ManufacturerDiscount) : base(speed, regularPrice, color)
        {
            this.Year = Year;
            this.ManufacturerDiscount = ManufacturerDiscount;
        }
        public override double GetSalePrice()
        {
            return RegularPrice - ManufacturerDiscount;
        }
        public void DisplayPrice()
        {
            Console.WriteLine( Color + "Ford Price: " + GetSalePrice());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    internal class GiaoVien
    {
        public int ID {  get; set; }
        public string Ten {  get; set; }
        public double SoGioDay {  get; set; }

        public GiaoVien()
        {
        }

        public GiaoVien(int iD, string ten, double soGioDay)
        {
            ID = iD;
            Ten = ten;
            SoGioDay = soGioDay;
        }
        public virtual void InThongTin()
        {
            Console.WriteLine("ID : " + ID + "   Ten: " + Ten + "   So Gio Day: " + SoGioDay);
        }
        public void Remove()
        {

        }
    }
}

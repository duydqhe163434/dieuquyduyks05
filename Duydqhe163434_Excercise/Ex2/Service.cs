﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex2
{
    internal class Service
    {
        List<GiaoVien> DanhSachGiaoVien = new List<GiaoVien>();
        public void NhapDanhSachGiaoVien()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                int id =  int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap ten : ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap so gio day: ");
                double soGioDay = double.Parse(Console.ReadLine());
                GiaoVien giaovien = new GiaoVien(id, ten, soGioDay);
                DanhSachGiaoVien.Add(giaovien);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachGiaoVien()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (GiaoVien Giaovien in DanhSachGiaoVien)
            {
                Giaovien.InThongTin();
            }

        }

        public void XuatDanhSachGiaoVienTheoGioDay()
        {   
            Console.WriteLine("Nhap thoi gian nho nhat: ");
            double gioMin = double.Parse(Console.ReadLine());
            Console.WriteLine("Nhap thoi gian lon nhat:");
            double gioMax = double.Parse(Console.ReadLine());
            double gioDay = gioMax - gioMin;
            foreach (GiaoVien Giaovien in DanhSachGiaoVien)
            {
                if (gioDay == Giaovien.SoGioDay)
                {
                    Giaovien.InThongTin();
                }
            }
        }
        public void XoaGiaoVienTheoID()
        {

            Console.WriteLine("Nhap ID can xoa: ");
            int idXoa = int.Parse(Console.ReadLine());  
            for(int i = 0; i<DanhSachGiaoVien.Count(); i++)
            {
                if(idXoa == DanhSachGiaoVien[i].ID)
                {
                    DanhSachGiaoVien.Remove(DanhSachGiaoVien[i]);
                    i--;
                }
                Console.WriteLine();
            }

        }
    }
}

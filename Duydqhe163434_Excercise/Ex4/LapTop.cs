﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    internal class LapTop
    {
        public int ID {  get; set; }
        public string MaLapTop {  get; set; }
        public double KichThuocMH {  get; set; }

        public LapTop()
        {
        }

        public LapTop(int iD, string maLapTop, double kichThuocMH)
        {
            ID = iD;
            MaLapTop = maLapTop;
            KichThuocMH = kichThuocMH;
        }

        public void InThongTin()
        {
            Console.WriteLine("ID: " + ID + "   Ma LapTop" + MaLapTop + "   Kich Thuoc Man Hinh: " + KichThuocMH);
        }
    }
}

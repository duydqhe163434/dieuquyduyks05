﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex4
{
    internal class Service
    {
        List<LapTop> DanhSachLapTop = new List<LapTop>();
        public void NhapDanhSachLapTop()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap Ma Laptop: ");
                string maLapTop = Console.ReadLine();
                Console.WriteLine("Nhap kich thuoc man hinh: ");
                double kichThuocMH = double.Parse(Console.ReadLine());
                LapTop laptop = new LapTop(id, maLapTop, kichThuocMH);
                DanhSachLapTop.Add(laptop);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachLapTop()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (LapTop Laptop in DanhSachLapTop)
            {
                Laptop.InThongTin();
            }

        }
    }
}

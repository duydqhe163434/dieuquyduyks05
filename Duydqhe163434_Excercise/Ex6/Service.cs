﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex6
{
    internal class Service
    {
        List<HocSinh> DanhSachHocSinh = new List<HocSinh>();
        public void NhapDanhSachHocSinh()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ma HS: ");
                string maHS = Console.ReadLine();
                Console.WriteLine("Nhap ten HS: ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap tuoi: ");
                int tuoi = Convert.ToInt32(Console.ReadLine());
                HocSinh hocsinh = new HocSinh(maHS, ten, tuoi);
                DanhSachHocSinh.Add(hocsinh);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachHocSinh()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (HocSinh Hocsinh in DanhSachHocSinh)
            {
                Hocsinh.InThongTin();
            }

        }
       
    }
}

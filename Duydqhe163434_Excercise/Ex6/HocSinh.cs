﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex6
{
    internal class HocSinh
    {
        public string MaHS {  get; set; }
        public string Ten {  get; set; }
        public int Tuoi {  get; set; }

        public HocSinh()
        {
        }

        public HocSinh(string maHS, string ten, int tuoi)
        {
            MaHS = maHS;
            Ten = ten;
            Tuoi = tuoi;
        }

        public void InThongTin()
        {
            Console.WriteLine("Ma Hoc Sinh: " + MaHS + "   Ten: " + Ten + "   Tuoi: " + Tuoi);
        }
    }
}

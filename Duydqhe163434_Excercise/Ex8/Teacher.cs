﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex8
{
    internal class Teacher
    {
        public int ID { get; set; }
        public string MaGV { get; set; }
        public string Nganh {  get; set; }

        public Teacher()
        {
        }

        public Teacher(int iD, string maGV, string nganh)
        {
            ID = iD;
            MaGV = maGV;
            Nganh = nganh;
        }

        public void InThongTin()
        {
            Console.WriteLine("ID: " + ID + "   Ma giao vien: " + MaGV + "   Nganh: " + Nganh);
        }
    }
}

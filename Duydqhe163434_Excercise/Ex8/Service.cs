﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex8
{
    internal class Service
    {
        List<Teacher> DanhSachGiaoVien = new List<Teacher>();
        public void NhapDanhSachGiaoVien()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                int id = int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap ma giao vien : ");
                string maGV = Console.ReadLine();
                Console.WriteLine("Nhap nganh: ");
                string nganh = Console.ReadLine();
                Teacher giaovien = new Teacher(id, maGV, nganh);
                DanhSachGiaoVien.Add(giaovien);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachGiaoVien()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (Teacher Giaovien in DanhSachGiaoVien)
            {
                Giaovien.InThongTin();
            }

        }

       
    }
}


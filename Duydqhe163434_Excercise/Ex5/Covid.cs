﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex5
{
    internal class Covid
    {
        public string MaCovid {  get; set; }
        public string Ten {  get; set; }
        public int NamPhatTrien {  get; set; }

        public Covid()
        {
        }

        public Covid(string maCovid, string ten, int namPhatTrien)
        {
            MaCovid = maCovid;
            Ten = ten;
            NamPhatTrien = namPhatTrien;
        }
        public void InThongTin()
        {
            Console.WriteLine("Ma Covid: " + MaCovid + "   Ten" + Ten + "   Nam Phat Trien: " + NamPhatTrien);
        }
    }
}

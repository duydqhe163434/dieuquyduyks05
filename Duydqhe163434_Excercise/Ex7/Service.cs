﻿using Ex7;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex7
{
    internal class Service
    {
        List<Student> DanhSachHocSinh = new List<HocSinh>();
        public void NhapDanhSachHocSinh()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                string id = Console.ReadLine();
                Console.WriteLine("Nhap ten HS: ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap tuoi: ");
                int tuoi = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Nhap nganh: ");
                string nganh = Console.ReadLine();
                Student hocsinh = new HocSinh(id, ten, tuoi,nganh);
                DanhSachHocSinh.Add(hocsinh);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachHocSinh()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (Student Hocsinh in DanhSachHocSinh)
            {
                Hocsinh.InThongTin();
            }

        }

    }
}

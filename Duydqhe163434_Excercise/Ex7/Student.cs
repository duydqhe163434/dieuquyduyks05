﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex7
{
    internal class Student
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public int Tuoi { get; set; }
        public string Nganh { get; set; }

        public Student()
        {
        }

        public Student(int iD, string ten, int tuoi, string nganh)
        {
            ID = iD;
            Ten = ten;
            Tuoi = tuoi;
            Nganh = nganh;
        }
        public void InThongTin()
        {
            Console.WriteLine("Ma Hoc Sinh: " + ID + "   Ten: " + Ten + "   Tuoi: " + Tuoi + "   Nganh: " + Nganh);
        }
    }
}

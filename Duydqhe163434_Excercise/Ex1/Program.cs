﻿// See https://aka.ms/new-console-template for more information
using Ex1;
using System.ComponentModel.Design;
using System.Runtime.Intrinsics.X86;
Service service = new Service();
do
{
    Console.WriteLine("Quan Li Sinh Vien");
    Console.WriteLine("1.Nhap danh sach doi tuong.");
    Console.WriteLine("2.Xuat danh sach doi tuong.");
    Console.WriteLine("3.Xuat danh sach cac sinh vien lon hon 50 tuoi.");
    Console.WriteLine("4.Tim sv theo ma.");
    Console.WriteLine("5.Ke thua.");
    Console.WriteLine("6.Thoat.");
    Console.WriteLine("Nhap lua chon: ");
    int choice = Convert.ToInt32(Console.ReadLine());
    if (choice >= 1 && choice <= 6)
    {
        switch (choice)
        {
            case 1:
                service.NhapDanhSachSinhVien();
                break;
            case 2:
                service.XuatDanhSachSinhVien();
                break;
            case 3:
                service.XuatDanhSachSinhVienLonHon50Tuoi();
                break;
            case 4:
                service.TimSinhVienTheoMa();
                break;
            case 5:
                service.KeThua();
                break;
            default:
                break;

        }
        
    }
    else
        {
        break;
    }
} while (true);

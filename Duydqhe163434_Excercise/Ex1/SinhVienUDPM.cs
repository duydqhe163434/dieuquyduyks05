﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1
{
    internal class SinhVienUDPM  :SinhVien
    {
        public double DiemJava {  get; set; }
        public double DiemCsharp { get; set; }

        public SinhVienUDPM()
        {
        }

        public SinhVienUDPM(string maSV, string ten, int namSinh, double DiemJava, double DiemCsharp) : base(maSV, ten, namSinh)
        {
            this.DiemJava = DiemJava;
            this.DiemCsharp = DiemCsharp;
        }

        public override void InThongTin()
        {
            Console.WriteLine("Mã SV: " + MaSV + "   Ten: " + Ten + "   Nam Sinh: " + NamSinh + "  Diem Java: " + DiemJava + "   Diem C#: " + DiemCsharp);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1
{
    internal class Service
    {
        List<SinhVien> DanhSachSinhVien = new List<SinhVien>();
        public void NhapDanhSachSinhVien()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ma SV: ");
                string maSv = Console.ReadLine();
                Console.WriteLine("Nhap ten SV: ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap nam sinh SV: ");
                int namSinh = Convert.ToInt32(Console.ReadLine());
                SinhVien sinhvien = new SinhVien(maSv, ten, namSinh);
                DanhSachSinhVien.Add(sinhvien);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachSinhVien()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (SinhVien Sinhvien in DanhSachSinhVien)
            {
                Sinhvien.InThongTin();
            }

        }
        public void XuatDanhSachSinhVienLonHon50Tuoi()
        {
            foreach (SinhVien Sinhvien in DanhSachSinhVien)
            {
                Sinhvien.TinhTuoi();
            }
        }

        public void TimSinhVienTheoMa()
        {
            Console.WriteLine("Nhap ma sinh vien: ");
            int MaSV1 = int.Parse(Console.ReadLine());  
            foreach(SinhVien Sinhvien in DanhSachSinhVien)
            {

                if (Sinhvien.MaSV.Equals(MaSV1))
                {
                    Console.WriteLine(MaSV1);
                }
            }
        }
        public void KeThua()
        {
            SinhVienUDPM sinhVienUDPM = new SinhVienUDPM("asasas", "duy",1950,7.8,7.0);
            sinhVienUDPM.InThongTin();

            
        }
    }
}
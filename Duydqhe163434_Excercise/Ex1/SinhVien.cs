﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex1
{
    internal class SinhVien
    {
        public string MaSV {  get; set; }
        public string Ten {  get; set; }
        public int NamSinh {  get; set; }

        public SinhVien()
        {
        }

        public SinhVien(string maSV, string ten, int namSinh)
        {
            MaSV = maSV;
            Ten = ten;
            NamSinh = namSinh;
        }
        public virtual void InThongTin()
        {
            Console.WriteLine("Mã SV: " + MaSV + "   Ten" + Ten + "   Nam Sinh" + NamSinh);
        }
        
        
        public void TinhTuoi() 
        {
            DateTime Hientai = DateTime.Now;
            int Tuoi = Hientai.Year - NamSinh;
            if(Tuoi > 50)
            {
                Console.WriteLine("Mã SV: " + MaSV + "   Ten: " + Ten + "   Nam Sinh: " + NamSinh + "    Tuoi: " + Tuoi);
            }
        }
         

    }
}

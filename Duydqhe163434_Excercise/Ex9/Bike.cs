﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex9
{
    internal class Bike
    {
        public int ID { get; set; }
        public string Ten { get; set; }
        public string HSX { get; set; }

        public Bike()
        {
        }

        public Bike(int iD, string ten, string hSX)
        {
            ID = iD;
            Ten = ten;
            HSX = hSX;
        }
        public void InThongTin()
        {
            Console.WriteLine("ID: " + ID + "   Ten: " + Ten + "    Hang san xuat: " + HSX);
        }

    }
}

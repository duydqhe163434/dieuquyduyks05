﻿using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex9
{
    internal class Service
    {
        List<Bike> DanhSachXe = new List<Bike>();
        public void NhapDanhSachXe()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                int iD = int.Parse(Console.ReadLine());
                Console.WriteLine("Nhap ten xe: ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap Hang san xuat: ");
                string hSX = Console.ReadLine();
                Bike bike = new Bike(iD, ten, hSX);
                DanhSachXe.Add(bike);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachXe()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (Bike BIke in DanhSachXe)
            {
                BIke.InThongTin();
            }

        }
    }
}

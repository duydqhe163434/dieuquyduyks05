﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex3
{
    internal class Service
    {
        List<MayTinh> DanhSachMayTinh = new List<MayTinh>();
        public void NhapDanhSachMayTinh()
        {
            string nhapTiep;
            do
            {
                Console.WriteLine("Nhap ID: ");
                string id = Console.ReadLine();
                Console.WriteLine("Nhap ten : ");
                string ten = Console.ReadLine();
                Console.WriteLine("Nhap so gio day: ");
                float trongLuong = float.Parse(Console.ReadLine());
                MayTinh maytinh = new MayTinh(id, ten, trongLuong );
                DanhSachMayTinh.Add(maytinh);
                Console.WriteLine("Nhap them y/n:");
                nhapTiep = Console.ReadLine();
                if (nhapTiep.Equals("n"))
                {
                    break;
                }
            } while (true);
        }
        public void XuatDanhSachMayTinh()
        {

            Console.WriteLine("Danh sach sinh vien:");
            foreach (MayTinh Maytinh in DanhSachMayTinh)
            {
                Maytinh.InThongTin();
            }

        }
    }
}
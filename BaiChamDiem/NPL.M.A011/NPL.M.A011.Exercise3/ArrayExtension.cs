﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A011.Exercise3
{
    internal static class ArrayExtension
    {
        public static int LastIndexOf<T>(this T[] array, T elementValue) where T : IComparable<T>
        {
            for (int i = array.Length - 1; i >= 0; i--)
            {
                if (object.Equals(array[i], elementValue))
                {
                    return i;
                }
            }
            return -1;
        }
    }
}


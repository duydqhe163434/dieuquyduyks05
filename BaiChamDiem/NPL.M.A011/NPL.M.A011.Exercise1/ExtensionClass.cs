﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A011.Exercise1
{
    public static class ExtensionClass
    {
        public static int CountInt(this ArrayList array)
        {
            return array.OfType<int>().Count();
        }
        
        public static int CountOf(this ArrayList array, Type dataType)
        {
            return array.Cast<object>().Count(item => dataType.IsInstanceOfType(item));
        }
       
        public static int CountOf<T>(this ArrayList array)
        {
            return array.OfType<T>().Count();
        }
       
        public static T MaxOf<T>(this ArrayList array) where T : IComparable<T>
        {
            if (IsNumericType(typeof(T)))
            {
                T max;
                bool check = false;
                for (int i = 0; i < array.Count; i++)
                {
                    if (array[i].GetType() == typeof(T))
                    {
                        check = true;
                        max = (T)array[i];
                        foreach (var item in array)
                        {
                            if (item is T tItem)
                            {
                                if (tItem.CompareTo(max) > 0)
                                {
                                    max = tItem;
                                }
                            }
                        }
                        return max;
                    }
                }
            }
            else
            {
                throw new InvalidOperationException("T must be a numeric data type.");
            }
            return (T)array[0];
        }

        private static bool IsNumericType(Type type)
        {
            return type.IsPrimitive && type != typeof(bool);
        }

    }
}



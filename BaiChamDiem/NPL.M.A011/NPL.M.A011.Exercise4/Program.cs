﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A011.Exercise4;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;

int[] numbers = { 3, 2, 5, 6, 1, 7, 7, 5, 2 };

int[] newArray = numbers.RemoveDuplicate<int>();

int secondLargest = newArray.SecondLargest();
int thirdLargest = newArray.OrderLargest(2);
int fourthLargest = newArray.OrderLargest(3);

Console.WriteLine("Phần tử thứ hai lớn nhất: " + secondLargest);
Console.WriteLine("Phần tử thứ ba lớn nhất: " + thirdLargest);
Console.WriteLine("Phần tử thứ tư lớn nhất: " + fourthLargest);


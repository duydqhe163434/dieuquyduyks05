﻿using NPL.M.A015.Exercise;
using System.Collections;
using System.Text.RegularExpressions;

namespace NPL.M.A015.Exercise
{
    internal class Manage
    {
        public static List<Employee> employees = new List<Employee>
        {
        new Employee(1,"Nam",1,12,"Son La",DateOnly.MinValue,true),
        new Employee(2,"Duy",1,12,"Phu Tho",DateOnly.MinValue,true),
        new Employee(3,"Thanh",1,12,"Ha Noi",DateOnly.MinValue,true),
        new Employee(4,"Dung", 2, 12, "Da Nang", DateOnly.MinValue, false)
        };
        public static List<Department> departments = new List<Department>
        {
            new Department(1,"Kinh Te"),
            new Department(2,"IT"),
            new Department(3,"Kinh Te Doi Ngoai")
        };
        public static List<ProgramingLaguage> programingLaguages = new List<ProgramingLaguage>
        {
            new ProgramingLaguage(1,"C#"),
            new ProgramingLaguage(2,"Python"),
            new ProgramingLaguage(3,"Java")
        };
        public static List<EmployeeSkill> employeeSkills = new List<EmployeeSkill>
        {
            new EmployeeSkill(1,1),
            new EmployeeSkill(4,1),
            new EmployeeSkill(1,2),
            new EmployeeSkill(1,3),
            new EmployeeSkill(2,3),
        };

        public void InputEmployees()
        {
            Console.WriteLine("=== Input Employees ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Employee ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Employee ID. Please try again.");
                    continue;
                }

                if (employees.Any(e => e.Id == id))
                {
                    Console.WriteLine("Employee ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Employee Name: ");
                string name = Console.ReadLine();

                Console.Write("Department ID: ");
                string deptIdInput = Console.ReadLine();

                if (!int.TryParse(deptIdInput, out int deptId))
                {
                    Console.WriteLine("Invalid Department ID. Please try again.");
                    continue;
                }

                Console.Write("Age: ");
                string ageInput = Console.ReadLine();

                if (!int.TryParse(ageInput, out int age))
                {
                    Console.WriteLine("Invalid Age. Please try again.");
                    continue;
                }

                Console.Write("Address: ");
                string address = Console.ReadLine();

                Console.Write("Hired Date (yyyy-MM-dd): ");
                string hiredDateInput = Console.ReadLine();

                if (!DateOnly.TryParse(hiredDateInput, out DateOnly hiredDate))
                {
                    Console.WriteLine("Invalid Hired Date. Please try again.");
                    continue;
                }

                Console.Write("Status (true/false): ");
                string statusInput = Console.ReadLine();

                if (!bool.TryParse(statusInput, out bool status))
                {
                    Console.WriteLine("Invalid Status. Please try again.");
                    continue;
                }

                Employee employee = new Employee(id, name, deptId, age, address, hiredDate, status);

                employees.Add(employee);

                Console.Write("Do you want to continue inputting employees? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }

        public void InputDepartments()
        {
            Console.WriteLine("=== Input Departments ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Department ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Department ID. Please try again.");
                    continue;
                }

                // Check if ID already exists
                if (departments.Any(d => d.Id == id))
                {
                    Console.WriteLine("Department ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Department Name: ");
                string name = Console.ReadLine();

                Department department = new Department(id, name);

                departments.Add(department);

                Console.Write("Do you want to continue inputting departments? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }

        public void InputEmployeeSkills()
        {
            Console.WriteLine("=== Input Employee Skills ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Employee ID: ");
                string empIdInput = Console.ReadLine();

                if (!int.TryParse(empIdInput, out int empId))
                {
                    Console.WriteLine("Invalid Employee ID. Please try again.");
                    continue;
                }

                if (!employees.Any(e => e.Id == empId))
                {
                    Console.WriteLine("Employee ID does not exist. Please enter a valid Employee ID.");
                    continue;
                }

                Console.Write("Language ID: ");
                string langIdInput = Console.ReadLine();

                if (!int.TryParse(langIdInput, out int langId))
                {
                    Console.WriteLine("Invalid Language ID. Please try again.");
                    continue;
                }

                if (!programingLaguages.Any(l => l.Id == langId))
                {
                    Console.WriteLine("Language ID does not exist. Please enter a valid Language ID.");
                    continue;
                }

                if (employeeSkills.Any(es => es.EmpID == empId && es.LangID == langId))
                {
                    Console.WriteLine("Employee already has this Language skill. Please enter a different Language ID.");
                    continue;
                }

                EmployeeSkill employeeSkill = new EmployeeSkill(empId, langId);

                employeeSkills.Add(employeeSkill);

                Console.Write("Do you want to continue inputting employee skills? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }
        public void InputProgrammingLanguages()
        {
            Console.WriteLine("=== Input Programming Languages ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Language ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Language ID. Please try again.");
                    continue;
                }

                if (programingLaguages.Any(pl => pl.Id == id))
                {
                    Console.WriteLine("Language ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Language Name: ");
                string name = Console.ReadLine();
                ProgramingLaguage programmingLanguage = new ProgramingLaguage(id, name);
                programingLaguages.Add(programmingLanguage);

                Console.Write("Do you want to continue inputting programming languages? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }
        public List<Department> GetDepartments(int numberOfEmployees)
        {
            var departmentsWithEmployees = (from dp in departments
                                            join ep in employees on dp.Id equals ep.DeparmentID
                                            group ep by new { dp.Id, dp.Name } into g
                                            where g.Count() > numberOfEmployees
                                            select new Department
                                            {
                                                Id = g.Key.Id,
                                                Name = g.Key.Name,
                                            }).ToList();

            return departmentsWithEmployees;
        }
        public List<Employee> GetEmployeesWorking()
        {
            var workingEmployees = employees.Where(e => e.Status).ToList();
            return workingEmployees;
        }
        public List<Employee> GetEmployees(string languageName)
        {
            var employeesWithLanguage = (from emp in employees
                                         join empSkill in employeeSkills on emp.Id equals empSkill.EmpID
                                         join lang in programingLaguages on empSkill.LangID equals lang.Id
                                         where lang.Name == languageName
                                         select emp).Distinct().ToList();
            return employeesWithLanguage;
        }
        public List<Employee> GetSeniorEmployees()
        {
            var seniorEmployees = (from emp in employees
                                   join empSkill in employeeSkills on emp.Id equals empSkill.EmpID
                                   group emp by emp.Id into g
                                   where g.Count() > 1
                                   select g.Key).ToList();

            var seniorEmployeesList = employees.Where(e => seniorEmployees.Contains(e.Id)).ToList();
            return seniorEmployeesList;
        }
        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var filteredEmployees = employees;
            if (!string.IsNullOrEmpty(employeeName))
            {
                filteredEmployees = filteredEmployees.Where(e => e.Name.Contains(employeeName)).ToList();
            }

            if (order == "ASC")
            {
                filteredEmployees = filteredEmployees.OrderBy(e => e.Name).ToList();
            }
            else if (order == "DESC")
            {
                filteredEmployees = filteredEmployees.OrderByDescending(e => e.Name).ToList();
            }

            int startIndex = (pageIndex - 1) * pageSize;

            var pagedEmployees = filteredEmployees.Skip(startIndex).Take(pageSize).ToList();

            return pagedEmployees;
        }
        public string GetLanguages(int employeeId)
        {
            string lang = "";
            foreach (EmployeeSkill employeeSkill in employeeSkills)
            {
                if (employeeSkill.EmpID == employeeId)
                {
                    foreach (ProgramingLaguage language in programingLaguages)
                    {
                        if (language.Id == employeeSkill.LangID)
                        {
                            lang += language.Name + " ";
                        }
                    }
                }
            }

            return lang;
        }
        public Hashtable GetDepartments()
        {
            var departmentHashtable = new Hashtable();

            foreach (var dept in departments)
            {
                var employeesInDepartment = employees.Where(emp => emp.DeparmentID == dept.Id).ToList();
                departmentHashtable.Add(dept, employeesInDepartment);
            }

            return departmentHashtable;
        }
    }
}


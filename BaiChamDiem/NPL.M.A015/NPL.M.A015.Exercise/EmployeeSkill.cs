﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class EmployeeSkill
    {
        public int EmpID { get; set; }
        public int LangID { get; set; }

        public EmployeeSkill()
        {
        }

        public EmployeeSkill(int empID, int langID)
        {
            EmpID = empID;
            LangID = langID;
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class ProgramingLaguage
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ProgramingLaguage()
        {
        }

        public ProgramingLaguage(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Department() 
        {

        }
        public Department(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}




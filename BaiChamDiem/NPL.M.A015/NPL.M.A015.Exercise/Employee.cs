﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int DeparmentID { get; set; }
        public int Age { get; set; }
        public string Address { get; set; }
        public DateOnly HiredDate { get; set; }
        public bool Status { get; set; }

        public Employee()
        {
        }

        public Employee(int id, string name, int deparmentID, int age, string address, DateOnly hiredDate, bool status)
        {
            Id = id;
            Name = name;
            DeparmentID = deparmentID;
            Age = age;
            Address = address;
            HiredDate = hiredDate;
            Status = status;
        }
    }
}




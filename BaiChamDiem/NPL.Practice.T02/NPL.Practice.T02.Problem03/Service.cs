﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace NPL.Practice.T02.Problem03
{
    internal class Service
    {
        public List<Student> students = new List<Student>();

        public void InputStudentInfo()
        {
            Console.WriteLine("Nhập thông tin sinh viên:");

            int id = -1;
            bool isIdUnique;

            do
            {
                Console.Write("ID: ");
                string idInput = Console.ReadLine();

                try
                {
                    id = int.Parse(idInput);

                    
                    isIdUnique = true;
                    foreach (Student existingStudent in students)
                    {
                        if (existingStudent.Id == id)
                        {
                            isIdUnique = false;
                            Console.WriteLine("ID đã tồn tại. Vui lòng nhập ID khác.");
                            break;
                        }
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("ID không hợp lệ. Vui lòng nhập số nguyên.");
                    isIdUnique = false; 
                }
            } while (!isIdUnique);

            Console.Write("Tên: ");
            string name = Console.ReadLine();

            DateTime startDate;
            do
            {
                try
                {
                    Console.Write("Ngày bắt đầu (yyyy-MM-dd): ");
                    startDate = DateTime.Parse(Console.ReadLine());
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Lỗi định dạng ngày. Hãy kiểm tra lại dữ liệu đã nhập.");
                    continue;
                }
                break;
            } while (true);

            decimal sqlMark, csharpMark, dsaMark;

            do
            {
                Console.Write("Điểm SQL: ");
                string sqlMarkInput = Console.ReadLine();
                try
                {
                    sqlMark = decimal.Parse(sqlMarkInput);
                    if (sqlMark < 0 || sqlMark > 10)
                    {
                        Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số từ 0 đến 10.");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số hợp lệ.");
                }
            } while (true);

            do
            {
                Console.Write("Điểm C#: ");
                string csharpMarkInput = Console.ReadLine();
                try
                {
                    csharpMark = decimal.Parse(csharpMarkInput);
                    if (csharpMark < 0 || csharpMark > 10)
                    {
                        Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số từ 0 đến 10.");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số hợp lệ.");
                }
            } while (true);

            do
            {
                Console.Write("Điểm DSA: ");
                string dsaMarkInput = Console.ReadLine();
                try
                {
                    dsaMark = decimal.Parse(dsaMarkInput);
                    if (dsaMark < 0 || dsaMark > 10)
                    {
                        Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số từ 0 đến 10.");
                    }
                    else
                    {
                        break;
                    }
                }
                catch (FormatException)
                {
                    Console.WriteLine("Điểm không hợp lệ. Vui lòng nhập số hợp lệ.");
                }
            } while (true);

            Student student = new Student(id, name, startDate, sqlMark, csharpMark, dsaMark);
            student.CalculateGPA();
            student.Graduate();
            students.Add(student);
        }

    }
}

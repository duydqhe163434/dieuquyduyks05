﻿using NPL.Practice.T02.Problem03;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

// 1. Tạo enum GraduateLevel với 5 giá trị: Excellent, Very Good, Good, Average, Failed.

public class Student : IGraduate
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateTime StartDate { get; set; }
    public decimal SqlMark { get; set; }
    public decimal CsharpMark { get; set; }
    public decimal DsaMark { get; set; }
    public decimal GPA { get; set; }
    internal GraduateLevel GraduateLevel { get; set; }

    public Student()
    {
    }

    internal Student(int id, string name, DateTime startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark)
    {
        Id = id;
        Name = name;
        StartDate = startDate;
        SqlMark = sqlMark;
        CsharpMark = csharpMark;
        DsaMark = dsaMark;
    }

    public void Graduate()
    {
        CalculateGPA();
        if (GPA >= 9)
            GraduateLevel = GraduateLevel.Excellent;
        else if (GPA >= 8)
            GraduateLevel = GraduateLevel.VeryGood;
        else if (GPA >= 7)
            GraduateLevel = GraduateLevel.Good;
        else if (GPA >= 5)
            GraduateLevel = GraduateLevel.AverageGood;
        else
            GraduateLevel = GraduateLevel.Failed;
    }

    public void CalculateGPA()
    {
        GPA = (SqlMark + CsharpMark + DsaMark) / 3;
    }

    public string GetCertificate()
    {
        return "Ten: " + Name + "\n"
            + "SqlMark: " + SqlMark + "\n"
            + "CsharpMark: " + CsharpMark + "\n"
            + "DsaMark: " + DsaMark + "\n"
            + "GPA: " + GPA + "\n"
            + "GraduateLevel: " + GraduateLevel; 
    }


}

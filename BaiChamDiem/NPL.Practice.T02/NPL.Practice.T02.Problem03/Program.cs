﻿// See https://aka.ms/new-console-template for more information
using NPL.Practice.T02.Problem03;

Console.OutputEncoding = System.Text.Encoding.Unicode;
Service service = new Service();
bool isRunning = true;

while (isRunning)
{
    Console.WriteLine("---- Quản lý sinh viên ----");
    Console.WriteLine("1. Nhập thông tin sinh viên");
    Console.WriteLine("2. In thông tin chứng chỉ");
    Console.WriteLine("3. Thoát");

    Console.Write("Chọn tùy chọn: ");
    string choice = Console.ReadLine();

    switch (choice)
    {
        case "1":
            service.InputStudentInfo();
            Console.WriteLine("Đã nhập thông tin sinh viên.");
            break;
        case "2":
            if (service.students.Count()>0)
            {
                foreach(Student student in service.students)
                {
                    Console.WriteLine(student.GetCertificate());
                }
            }
            else
            {
                Console.WriteLine("Vui lòng nhập thông tin sinh viên và tính toán GraduateLevel trước.");
            }
            break;

        case "3":
            isRunning = false;
            break;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
            break;
    }
}
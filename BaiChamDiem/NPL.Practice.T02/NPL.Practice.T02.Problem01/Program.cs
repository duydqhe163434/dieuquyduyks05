﻿// See https://aka.ms/new-console-template for more information

using System.Threading.Channels;
Console.OutputEncoding = System.Text.Encoding.Unicode;
Console.WriteLine("Nhap chuoi:");
string contentOfArticle = Console.ReadLine();
int maxLength;
do
{
    try
    {
        Console.WriteLine("Nhap so ki tu trong chuoi con");
        maxLength = int.Parse(Console.ReadLine());
        break;
    }
    catch (Exception e)
    {
        Console.WriteLine("Vui long nhap lai");
    }
} while (true);
string summary = GetArticleSummary(contentOfArticle, maxLength);
Console.WriteLine(summary);
static string GetArticleSummary(string contentOfArticle, int maxLength)
{
    //Nếu số lượng phần tử trong chuỗi cần cắt lớn hơn chuỗi ban đầu
    if (maxLength > contentOfArticle.Length)
    {
        return contentOfArticle.Trim();
    }
    // Nếu mà vị trí cuối là khoảng trắng
    if (contentOfArticle[maxLength - 1] == ' ')
    {
        return contentOfArticle.Substring(0, maxLength - 1).Trim() + "...";
    }
    //Nếu mà vị trí cuối ko phải khoảng trắng và vị trí tiếp theo là khoảng trắng
    if (contentOfArticle[maxLength - 1] != ' ' && contentOfArticle[maxLength] == ' ')
    {
        return contentOfArticle.Substring(0, maxLength).Trim() + "...";
    }
    //Mếu mà vị trí cuối ko phải khoảng trắng và vị trí tiếp theo cũng ko phải 
    int index;
    if (contentOfArticle[maxLength - 1] != ' ' && contentOfArticle[maxLength] != ' ')
    {
        for (int i = maxLength - 1; i >= 0; i--)
        {
            if (contentOfArticle[i] == ' ')
            {
                return contentOfArticle.Substring(0, i).Trim() + "...";
            }
        }
    }
    return "...";
}



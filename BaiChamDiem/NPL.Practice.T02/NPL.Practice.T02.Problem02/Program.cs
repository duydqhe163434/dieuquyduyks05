﻿// See https://aka.ms/new-console-template for more information
Console.OutputEncoding = System.Text.Encoding.Unicode;
int subLength = 0;
int arrayLength = 0;
//Nhập các giá trị đầu vào
do
{
    try
    {
        Console.Write("Nhập độ dài của mảng: ");
        arrayLength = int.Parse(Console.ReadLine());
        break;
    }
    catch (Exception e)
    {
        Console.WriteLine("vui long nhập lại");
    }
} while (arrayLength <= 0);

int[] inputArray = new int[arrayLength];
for (int i = 0; i < arrayLength; i++)
{
    do
    {
        try
        {
            Console.Write("arr[" + (i + 1) + "]: ");
            inputArray[i] = int.Parse(Console.ReadLine());
            break;
        }
        catch (Exception e)
        {
            Console.WriteLine("Vui lòng nhập lại");
        }
    } while (true);
}

do
{
    try
    {
        Console.Write("Nhập độ dài của dãy con: ");
        subLength = int.Parse(Console.ReadLine());
        break;
    }
    catch (Exception e)
    {
        Console.WriteLine("Vui lòng nhập lại");
    }
} while (subLength <= 0);
int result = FindMaxSubArray(inputArray, subLength);
Console.WriteLine("Tổng lớn nhất của các phần tử trong mảng con: " + result);
static int FindMaxSubArray(int[] inputArray, int subLength)
{
    int maxSum = 0;
    int currentSum = 0;

    // Tính tổng các phần tử 
    for (int i = 0; i < subLength; i++)
    {
        currentSum += inputArray[i];
    }

    maxSum = currentSum;

    // Lặp lại mảng để tìm tổng lớn nhất, loại bỏ phần tử đầu tiên của dãy con trước và cộng vào phần tử đầu tiên của dãy con sau.
    // So sánh tổng cũ vs tổng mới ta sẽ có tổng các phần tử trong mảng con lớn nhất
    for (int i = subLength; i < inputArray.Length; i++)
    {
        currentSum -= inputArray[i - subLength];
        currentSum += inputArray[i];
        maxSum = Math.Max(maxSum, currentSum);
    }

    return maxSum;
}


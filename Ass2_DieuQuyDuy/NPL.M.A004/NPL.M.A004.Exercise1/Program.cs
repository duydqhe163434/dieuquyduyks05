﻿// See https://aka.ms/new-console-template for more information
using System.Globalization;

internal class Program
{
    public static DateTime GetLastDayOfMonth(DateTime date)
    {
       return new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
    }

    public static int CountWorkingDays(DateTime date)
    {
        int workingDay = 0;
        DateTime firstDay= new DateTime(date.Year, date.Month, 1);
        DateTime lastDay= new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));
        for(DateTime i= firstDay; i<= lastDay; i= i.AddDays(1))
        {
            if (i.DayOfWeek != DayOfWeek.Sunday && i.DayOfWeek != DayOfWeek.Saturday)
            {
                workingDay++;
            }
        }
        return workingDay;
    }

    private static void Main(string[] args)
    {
        string dateString = Console.ReadLine();
        string[] formats = { "MM/dd/yyyy", "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
                              "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",  "M/d/yyyy h:mm", "M/d/yyyy h:mm" };
        try
        {
            DateTime date = DateTime.ParseExact(dateString, formats, null);
            Console.WriteLine("Last day of month: ");
            Console.WriteLine(GetLastDayOfMonth(date).ToString("MM/dd/yyyy"));

            Console.WriteLine("Working day: ");
            Console.WriteLine(CountWorkingDays(date));

        }
        catch
        {
            Console.WriteLine("Invalid format date");
        }
    }
}


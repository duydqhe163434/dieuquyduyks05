﻿// See https://aka.ms/new-console-template for more information
using System.Text.RegularExpressions;

static bool IsEmail(string email)
{
    string parrtern = "^[a-zA-Z0-9!#$%&'*+\\/=?^_{|}~-]+\\.*@[a-zA-Z0-9]+-*(?=[a-zA-Z0-9]+)$";
    return Regex.IsMatch(email, parrtern);
}

string email = Console.ReadLine();
if (IsEmail(email))
{
    Console.WriteLine("Email is valid");
}
else
{
    Console.WriteLine("Email is invalid");
}


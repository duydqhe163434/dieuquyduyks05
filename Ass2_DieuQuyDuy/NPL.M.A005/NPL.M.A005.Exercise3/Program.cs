﻿// See https://aka.ms/new-console-template for more information
string inputName = Console.ReadLine();
string[] arr = inputName.Split(",");
for (int i = 0; i < arr.Length; ++i)
{
    arr[i] = arr[i].Trim();
}
string[] sortArr = SortName(arr);
Console.WriteLine();
for (int i = 0; i < sortArr.Length; i++)
{
    if (i < sortArr.Length - 1)
    {
        Console.Write(arr[i] + ", ");
    }
    else Console.Write(arr[i]);
}

static string[] SortName(string[] arr)
{
    string tmp;
    for (int i = 0; i < arr.Length; i++)
    {
        for (int j = i + 1; j < arr.Length; ++j)
        {
            string name1 = arr[i].Split(" ")[arr[i].Split(" ").Length - 1];
            string name2 = arr[j].Split(" ")[arr[j].Split(" ").Length - 1];
            if (name1.CompareTo(name2) > 0)
            {
                tmp = arr[j];
                arr[j] = arr[i];
                arr[i] = tmp;
            }
        }
    }
    return arr;
}




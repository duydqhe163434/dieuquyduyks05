﻿// See https://aka.ms/new-console-template for more information
using System.Text;

string name = Console.ReadLine();
name = name.ToLower();
while (name.IndexOf("  ") != -1) // kiểm tra có hai khoảng trắng liên tiếp trong name. 
{
    name = name.Replace("  ", " "); //thay thế hai khoảng trắng liên tiếp bằng một khoảng trắng.
}
StringBuilder fullName = new StringBuilder("");
string[] listWord = name.Split(" ");
for (int i = 0; i < listWord.Length; i++)
{
    string subName = listWord[i].Substring(0, 1).ToUpper() + listWord[i].Substring(1);//chuyển đổi từng từ thành chữ hoa chữ cái đầu tiên và kết hợp nó với phần còn lại của từ.
    fullName.Append(subName + " ");// thêm từ đã được chuyển đổi vào fullName, sau đó thêm một khoảng trắng sau từ đó.
}
Console.WriteLine(fullName);


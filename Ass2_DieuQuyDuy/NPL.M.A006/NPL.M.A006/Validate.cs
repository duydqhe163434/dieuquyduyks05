﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace NPL.M.A006
{
    internal class Validate
    {
        public string inputString(string message, string pattern)
        {
            string input;
            while (true)
            {
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (!Regex.IsMatch(input, pattern) || input.Equals(""))
                {
                    Console.WriteLine("Invalid input");
                    continue;
                }
                return input;
            }

        }

        public DateTime inputDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                string intput = Console.ReadLine();
                try
                {
                    DateTime date = DateTime.ParseExact(intput, "dd/MM/yyyy", null);
                    return date;
                }
                catch
                {
                    Console.WriteLine("Invalid format date: ");
                }
            }

        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Numerics;
using System.Runtime.Intrinsics.X86;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A006
{
    internal class Manager
    {
        Validate validate = new Validate();
        List<Employee> listEmployees = new List<Employee>();

        public void ImportEmployee()
        {
            Console.WriteLine("========= Import Employee =========");
            Console.WriteLine("1. Salaried Employee");
            Console.WriteLine("2. Hourly Employee");
            Console.WriteLine("3. Main Menu");
            Console.Write("Enter Menu Option Number: ");
            int choice = int.Parse(Console.ReadLine());
            if (choice == 1 || choice == 2)
            {
                string ssn = validate.inputString("Enter SSN: ", "^[A-Za-z0-9]+$");
                string firstName = validate.inputString("Enter First Name: ", "^[A-Za-z\\s]+$");
                string lastName = validate.inputString("Enter Last Name: ", "^[A-Za-z\\s]+$");
                DateTime birthDate = validate.inputDate("Enter BirthDate: ");
                string phone = validate.inputString("Enter Phone:", "^\\d{7,}$");
                string email = validate.inputString("Enter email:", "^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter commissionRate: ");
                        double commissionRate = double.Parse(Console.ReadLine());
                        Console.WriteLine("Enter grossSales: ");
                        double grossSales = double.Parse(Console.ReadLine());
                        Console.WriteLine("Enter basicSalary: ");
                        double basicSalary = double.Parse(Console.ReadLine());
                        SalariedEmployee salariedEmployee = new SalariedEmployee(ssn, firstName, lastName, birthDate, phone, email, commissionRate, grossSales, basicSalary);
                        listEmployees.Add(salariedEmployee);
                        break;
                    case 2:
                        Console.WriteLine("Enter wage: ");
                        double wage = double.Parse(Console.ReadLine());
                        Console.WriteLine("Enter workingHour: ");
                        double workingHour = double.Parse(Console.ReadLine());
                        HourlyEmployee hourlyEmployee = new HourlyEmployee(ssn, firstName, lastName, birthDate, phone, email, wage, workingHour);
                        listEmployees.Add(hourlyEmployee);
                        break;
                }
            }
        }

        public void DisplayEmployee()
        {
            int hourlyEmployeeCount = 0;
            int salariedEmployeeCount = 0;
            Console.WriteLine("Hourly Employee:");
            Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} ",
                                      "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
            foreach (Employee employee in listEmployees)
            {
                if (employee is HourlyEmployee)
                {
                    hourlyEmployeeCount = 1;
                    HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                    Console.WriteLine(hourlyEmployee.ToString());
                }
            }
            if (hourlyEmployeeCount == 0) Console.WriteLine("Not Found");
            Console.WriteLine();
            Console.WriteLine("Salaried Employee");
            Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} {8,-5} ",
                          "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "comminssionRate", "grossSales", "basicSalary"));
            foreach (Employee employee in listEmployees)
            {
                if (employee is SalariedEmployee)
                {
                    salariedEmployeeCount = 1;
                    SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                    Console.WriteLine(salariedEmployee.ToString());
                }
            }
            if (salariedEmployeeCount == 0) Console.WriteLine("Not Found");
            Console.WriteLine();
        }

        public void SearchEmployee()
        {
            Console.WriteLine("========= Search Employee =========");
            Console.WriteLine("1. By Employee Type");
            Console.WriteLine("2. By Employee Name");
            Console.WriteLine("3. Main Menu");
            Console.Write("Enter Menu Option Number: ");
            int choice = int.Parse(Console.ReadLine());
            if (choice == 1)
            {
                Console.Write("Enter Employee Type: ");
                string employeeType = Console.ReadLine();
                if (employeeType.Equals("Hourly") || employeeType.Equals("hourly"))
                {
                    Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} ",
                                              "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
                    foreach (Employee employee in listEmployees)
                    {
                        if (employee is HourlyEmployee)
                        {
                            HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                            Console.WriteLine(hourlyEmployee.ToString());
                        }
                    }
                }
                if (employeeType.Equals("Salaried") || employeeType.Equals("salaried"))
                {
                    Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} {8,-5} ",
                "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "comminssionRate", "grossSales", "basicSalary"));
                    foreach (Employee employee in listEmployees)
                    {
                        if (employee is SalariedEmployee)
                        {
                            SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                            Console.WriteLine(salariedEmployee.ToString());
                        }
                    }
                }
            }
            else if (choice == 2)
            {
                int hourlyEmployeeCount = 0;
                int salariedEmployeeCount = 0;
                Console.Write("Enter Employee Name: ");
                string employeeName = Console.ReadLine();
                Console.WriteLine("Hourly Employee:");
                Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} ",
                            "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "Wage", "Working Hours"));
                foreach (Employee employee in listEmployees)
                {
                    if (employee is HourlyEmployee && employee.firstName.Equals(employeeName) || employee.lastName.Equals(employeeName))
                    {
                        hourlyEmployeeCount = 1;
                        HourlyEmployee hourlyEmployee = (HourlyEmployee)employee;
                        Console.WriteLine(hourlyEmployee.ToString());
                    }
                }
                if (hourlyEmployeeCount == 0) Console.WriteLine("Not Found");
                Console.WriteLine("Salaried Employee");
                Console.WriteLine(string.Format("{0,-10}  {1,-15} {2,-15} {3,-10} {4,-10} {5,-20} {6,-5} {7,-5} {8,-5} ",
                "SSN", "FirstName", "LastName", "BirthDate", "Phone", "Email", "comminssionRate", "grossSales", "basicSalary"));
                foreach (Employee employee in listEmployees)
                {
                    if (employee is SalariedEmployee && employee.firstName.Equals(employeeName) || employee.lastName.Equals(employeeName))
                    {
                        salariedEmployeeCount = 1;
                        SalariedEmployee salariedEmployee = (SalariedEmployee)employee;
                        Console.WriteLine(salariedEmployee.ToString());
                    }
                }
                if (salariedEmployeeCount == 0) Console.WriteLine("Not Found");
            }
        }

        public void InitData()
        {
            listEmployees.Add(new HourlyEmployee("1", "Quy", "Duy", new DateTime(2023, 01, 01), "0111111111", "duy@gmail.com", 2, 3));
            listEmployees.Add(new SalariedEmployee("2", "Nhat", "Linh", new DateTime(2023, 01, 02), "0222222222", "linh@gmail.com", 2, 2, 2));
            listEmployees.Add(new HourlyEmployee("3", "Van", "Tuan", new DateTime(2023, 01, 03), "0333333333", "tuan@gmail.com", 2, 3));
            listEmployees.Add(new SalariedEmployee("4", "Trung", "Kien", new DateTime(2023, 01, 04), "044444444", "kien@gmail.com", 2, 2, 2));
            listEmployees.Add(new HourlyEmployee("5", "Bao", "Thanh", new DateTime(2023, 01, 05), "0555555", "thanh@gmail.com", 2, 3));
            listEmployees.Add(new SalariedEmployee("6", "Minh", "Dinh", new DateTime(2023, 01, 06), "0666666666", "Dinh@gmail.com", 2, 2, 2));
        }
    }
}



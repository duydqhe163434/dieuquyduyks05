﻿// See https://aka.ms/new-console-template for more information
using System.Globalization;

string dateString = Console.ReadLine();
try
{
    DateTime date = DateTime.ParseExact(dateString, "dd/MM/yyyy", null);
    int count = 0;
    while (count < 5)
    {
        count++;
        //Vì không tính ngày làm việc nếu là thứ 7 thì ngày được đếm sẽ +2 còn chủ nhật sẽ +1 đến khi bộ đếm bằng 7 thì 
        // chuyển sang lần report2
        if (count == 1)
        {
            for (int i = 0; i < 7; i++)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date = date.AddDays(2);
                }
                else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date = date.AddDays(1);
                }
            }
            Console.WriteLine("1st reminder: " + date.ToString("dd/MM/yyyy"));
        }
        // lần report 2 được gửi nếu sau 2 ngày rp 1 được gửi mà ko dc trả lời
        if (count == 2)
        {
            for (int i = 0; i < 2; i++)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date = date.AddDays(2);
                }
                else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date = date.AddDays(1);
                }
            }
            Console.WriteLine("2st reminder: " + date.ToString("dd/MM/yyyy"));
        }
        //Nếu 2<count<5 thì vẫn áp dụng logic ở trên và gửi rp liên tuc
        if (count > 2)
        {
            for (int i = 0; i < 1; i++)
            {
                date = date.AddDays(1);
                if (date.DayOfWeek.Equals(DayOfWeek.Saturday))
                {
                    date = date.AddDays(2);
                }
                else if (date.DayOfWeek.Equals(DayOfWeek.Sunday))
                {
                    date = date.AddDays(1);
                }
            }
            Console.WriteLine(count + "st reminder: " + date.ToString("dd/MM/yyyy"));
        }
    }
}
catch
{
    Console.WriteLine("Invalid format date");
}


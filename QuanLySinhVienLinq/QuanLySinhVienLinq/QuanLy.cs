﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace QuanLySinhVienLinq
{
    internal class QuanLy
    {
        List<SinhVien> danhSachSinhVien = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Nguyễn Văn A", Tuoi = 20, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Trần Thị B", Tuoi = 22, Khoa = "Toán học" },
            new SinhVien { MaSinhVien = 3, HoTen = "Lê Văn C", Tuoi = 21, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 4, HoTen = "Phạm Văn D", Tuoi = 23, Khoa = "Vật lý" },
            new SinhVien { MaSinhVien = 5, HoTen = "Hoàng Thị E", Tuoi = 24, Khoa = "Toán học" }
        };

        List<MonHoc> danhSachMonHoc = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "Lập trình cơ bản" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "Toán cao cấp" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "Vật lý cơ bản" }
        };

        List<GhiDanh> danhSachGhiDanh = new List<GhiDanh>
        {
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 101, Diem = 10 },
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 102, Diem = 9 },
            new GhiDanh { MaSinhVien = 2, MaMonHoc = 101, Diem = 8 },
            new GhiDanh { MaSinhVien = 3, MaMonHoc = 103, Diem = 7 },
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 102, Diem = 6 }
        };
        public void NhapThongTinSinhVien()
        {
            SinhVien sinhVien = new SinhVien();
            Console.WriteLine("Mời bạn nhập mã sinh viên");
            sinhVien.MaSinhVien = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập tên sinh viên");
            sinhVien.HoTen = Console.ReadLine();
            Console.WriteLine("Mời bạn nhập tuổi sinh viên");
            sinhVien.Tuoi = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập khoa");
            sinhVien.Khoa = Console.ReadLine();
            danhSachSinhVien.Add(sinhVien);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.Equals("y") || check.Equals("Y"))
            {
                NhapThongTinSinhVien();
            }
        }
        public void NhapThongTinMonHoc()
        {
            MonHoc monHoc = new MonHoc();
            Console.WriteLine("Mời bạn nhập mã môn học");
            monHoc.MaMonHoc = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập tên môn học");
            monHoc.TenMonHoc = Console.ReadLine();
            danhSachMonHoc.Add(monHoc);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.Equals("y") || check.Equals("Y"))
            {
                NhapThongTinMonHoc();
            }
        }
        public void ThemMonHoc()
        {
            GhiDanh ghiDanh = new GhiDanh();
            int checkSv = 0;
            int maSV = 0;
            int checkMH = 0;
            int maMH = 0;

            do
            {
                Console.WriteLine("Nhập mã sinh viên: ");
                maSV = int.Parse(Console.ReadLine());
                checkSv = danhSachSinhVien.Where(x => x.MaSinhVien == maSV).Count();

            } while (checkSv == 0);
            do
            {
                Console.WriteLine("Nhập mã môn học: ");
                maMH = int.Parse(Console.ReadLine());
                checkMH = danhSachMonHoc.Where(x => x.MaMonHoc == maMH).Count();

            } while (checkSv == 0);

            int checkDuyNhat = danhSachGhiDanh
                                .Where(x => (x.MaMonHoc == maMH) && (x.MaSinhVien == maSV))
                                .Count();
            if (checkDuyNhat == 1)
            {
                Console.WriteLine("Sinh viên đã đăng ký môn học");
                return;
            }

            ghiDanh.MaSinhVien = maSV;
            ghiDanh.MaMonHoc = maMH;
            Console.WriteLine("Nhập điểm sinh viên: ");
            ghiDanh.Diem = float.Parse(Console.ReadLine());
            danhSachGhiDanh.Add(ghiDanh);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.Equals("y") || check.Equals("Y"))
            {
                ThemMonHoc();
            }
        }
        public void TinhDiemTrungBinh()
        {
            foreach (SinhVien sinhVien in danhSachSinhVien)
            {
                List<GhiDanh> sinhVienGhiDanh = danhSachGhiDanh
                    .Where(gd => gd.MaSinhVien == sinhVien.MaSinhVien)
                    .ToList();

                if (sinhVienGhiDanh.Count > 0)
                {
                    double DTB = sinhVienGhiDanh.Average(gd => gd.Diem);
                    Console.WriteLine("Sinh vien " + sinhVien.HoTen + " co diem trung binh la: " + DTB);
                }
                else
                {
                    Console.WriteLine("Sinh vien " + sinhVien.HoTen + " chua dang ki mon hoc nao.");
                }
            }
        }
        public void ThongKeDanhSachSinhVienVaMonHoc()
        {
            var thongKeDanhSachSinhVienVaMonHoc = from gd in danhSachGhiDanh
                                                  join sv in danhSachSinhVien on gd.MaSinhVien equals sv.MaSinhVien
                                                  group gd by sv.HoTen into NhomTenSV
                                                  select new
                                                  {
                                                      SV = NhomTenSV.Key,
                                                      TongMonHoc = NhomTenSV.Count(),
                                                  };
            foreach (var sv in thongKeDanhSachSinhVienVaMonHoc)
            {
                Console.WriteLine("Sinh vien: " + sv.SV + " | So luong mon hoc: " + sv.TongMonHoc);
            }


            var rightJoinQuery = from leftItem in danhSachGhiDanh
                                 join rightItem in danhSachSinhVien
                                 on leftItem.MaSinhVien equals rightItem.MaSinhVien into rightGroup
                                 from result in rightGroup.DefaultIfEmpty()
                                 select new
                                 {
                                     LeftItem = leftItem,
                                     RightItem = result
                                 };
            foreach (var item in rightJoinQuery)
            {
                Console.WriteLine(item.LeftItem.MaSinhVien + "|" + item.RightItem.HoTen);
            }

            var query = from d in danhSachSinhVien
                        join e in danhSachGhiDanh
                        on d.MaSinhVien equals e.MaSinhVien into ed
                        select new
                        {
                            MaMH = d.MaSinhVien,
                            SoLuong = ed.Count()
                        };

            foreach (var result in query)
            {
                Console.WriteLine("Sinh Vien" + result.MaMH + " va so luong mon hoc " + result.SoLuong);
                
                
            }
        }
        public void ThongKeDanhMonHocVaSoSinhVien()
        {
            var thongKeDanhMonHocVaSoSinhVien = from gd in danhSachGhiDanh
                                                group gd by gd.MaMonHoc into NhomMaMH
                                                select new
                                                {
                                                    MH = NhomMaMH.Key,
                                                    TongSinhVien = NhomMaMH.Count(),
                                                };
            foreach (var mh in thongKeDanhMonHocVaSoSinhVien)
            {
                Console.WriteLine("Mon hoc: " + mh.MH + " | So luong sinh vien: " + mh.TongSinhVien);
            }
        }

        public void ThongKeTongSoLuongSinhVien()
        {
            int countSV = danhSachSinhVien.Count();
            Console.WriteLine("Tổng số sinh viên: " + countSV);
        }

        public void ThongKeMonHocCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from gd in danhSachGhiDanh
                                          group gd by gd.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };

            Console.WriteLine("So mon hoc co sinh vien: " + thongKeMonHocCoSinhVien.Count());
        }

        public void ThongKeMonHocKhongCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from gd in danhSachGhiDanh
                                          group gd by gd.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };
            Console.WriteLine("So mon hoc khong co sinh vien: " + (danhSachMonHoc.Count() - thongKeMonHocCoSinhVien.Count()));
        }
    }
}

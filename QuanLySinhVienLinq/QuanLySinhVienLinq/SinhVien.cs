﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVienLinq
{
    internal class SinhVien
    {
        public int MaSinhVien { get; set; }
        public string HoTen { get; set; }
        public int Tuoi { get; set; }
        public string Khoa { get; set; }
    }
}
